# Import socket module
import socket            
 
# Create a socket object
c = socket.socket()        
 
# Define the port on which you want to connect
port = 12345               
 
# connect to the server on local computer
c.connect(('127.0.0.1', port))
 
# receive data from the server and decoding to get the string.
while True:
    data = c.recv(1024).decode()
    if not data:
        break
    print(data)
    num = int(data)+1
    if num > 100:
        break
    c.send(str(num).encode())
    
# close the connection
c.close()